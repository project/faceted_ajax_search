

Drupal.faceted_ajax_search = Drupal.faceted_ajax_search || {};


/**
 * Event handler when search results and new serch blocks come back.
 */
Drupal.faceted_ajax_search.ajax_success_event = function(result) {
  $('#wrapper_faceted_ajax_result_region').hide();
  $('#wrapper_faceted_ajax_result_region').html(result.result);
  $('#wrapper_faceted_ajax_result_region').fadeIn('slow');

  for (var region in result.regions) {
    $('#faceted_ajax_search_wrapper_' + region).hide();
    $('#faceted_ajax_search_wrapper_' + region).html(result.regions[region]);
    $('#faceted_ajax_search_wrapper_' + region).fadeIn('slow');
    Drupal.attachBehaviors('#faceted_ajax_search_wrapper_' + region);
  }
  
  Drupal.attachBehaviors('#wrapper_faceted_ajax_result_region');
}

/**
 * Drupal behavior for faceted_ajax_search module.
 *
 * Attach ajax events for the selected search region.
 * Display results in the selected result region.
 */
Drupal.behaviors.faceted_ajax_search = function(context) {
  $('.block-faceted_search_ui a', context).each(function(){
    $(this).click(function(){
      var href = $(this).attr('href');
      // If link does not contain the word: results, it means it points to the
      // faceted starter page.
      if (!href.toString().match(/results/gi)) {
        href = window.location.href;
      }
      $('.block-faceted_search_ui').prepend('<div class="faceted_ajax_search_indicator"></div>');
      $.ajax({
        type: 'POST',
        url: href,
        data: 'faceted_ajax_search=1',
        dataType: 'json',
        success: Drupal.faceted_ajax_search.ajax_success_event
      });
      return false;
    });
  });

  $('.block-faceted_search_ui form', context).submit(function(){
    $('.block-faceted_search_ui').prepend('<div class="faceted_ajax_search_indicator"></div>');
    $.ajax({
      type: 'POST',
      url: $(this).attr('action'),
      data: $(this).serialize() + '&faceted_ajax_search=1&faceted_ajax_search_form=1',
      dataType: 'json',
      success: Drupal.faceted_ajax_search.ajax_success_event
    });
    return false;
  });
}

